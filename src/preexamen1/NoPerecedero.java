/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author safoe
 */
public class NoPerecedero extends Producto {
    private int LoteFabricacion;

    public NoPerecedero() {
        this.LoteFabricacion=0;
    }

    public NoPerecedero(int LoteFabricacion, int idProducto, String nombreProducto, int unidadProducto, int precioUnitario) {
        super(idProducto, nombreProducto, unidadProducto, precioUnitario);
        this.LoteFabricacion = LoteFabricacion;
    }
    
     public NoPerecedero(int LoteFabricacion) {
        this.LoteFabricacion = LoteFabricacion;
    }

    public int getLoteFabricacion() {
        return LoteFabricacion;
    }

    public void setLoteFabricacion(int LoteFabricacion) {
        this.LoteFabricacion = LoteFabricacion;
    }

    public float calcularProducto(){
        return getPrecioUnitario() * 1.50f;
    }

    public float calcularPago() {
       return calcularProducto();
    }
    
}
