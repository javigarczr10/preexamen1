/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author safoe
 */
public abstract class Producto {
    protected int idProducto;
    protected String nombreProducto;
    protected int unidadProducto;
    protected int precioUnitario;
    
    
    public Producto(){
        this.idProducto=0;
        this.nombreProducto="";
        this.unidadProducto=0;
        this.precioUnitario=0;
    }

    public Producto(int idProducto, String nombreProducto, int unidadProducto, int precioUnitario) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.unidadProducto = unidadProducto;
        this.precioUnitario = precioUnitario;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getUnidadProducto() {
        return unidadProducto;
    }

    public void setUnidadProducto(int unidadProducto) {
        this.unidadProducto = unidadProducto;
    }

    public int getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(int precioUnitario) {
        this.precioUnitario = precioUnitario;
    }


    
    public abstract float calcularPago();
}




