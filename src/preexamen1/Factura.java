/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author safoe
 */
public class Factura extends NotaVenta {
    private String rfc;
    private String nombre;
    private String domicilio;
    private String fechaFactura; 

    public Factura() {
    super();
    this.rfc = "";
    this.nombre = "";
    this.domicilio = "";
    this.fechaFactura = "";
}

    public Factura(String rfc, String nombre, String domicilio, String fechaFactura, int numNota, String fecha, String concepto, Producto producto, int cantidad, int tipoPago) {
        super(numNota, fecha, concepto, producto, cantidad, tipoPago);
        this.rfc = rfc;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fechaFactura = fechaFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }
    
      public float calcularIva() {
        float subtotal = super.calcularPago();
        return subtotal * 0.16f; 
    }
    @Override
    public float calcularPago() {
        float subtotal = super.calcularPago(); 
        float iva = calcularIva(); 
        return subtotal + iva; 
    }
    
}
