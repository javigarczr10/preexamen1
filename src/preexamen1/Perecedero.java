/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author safoe
 */
public class Perecedero extends Producto{
    private String FechaCaducidad;
    private float Temperatura;

    public Perecedero() {
        this.FechaCaducidad="";
        this.Temperatura=0.0f;
    }

    public Perecedero(String FechaCaducidad, float Temperatura, int idProducto, String nombreProducto, int unidadProducto, int precioUnitario) {
        super(idProducto, nombreProducto, unidadProducto, precioUnitario);
        this.FechaCaducidad = FechaCaducidad;
        this.Temperatura = Temperatura;
    }
    

    public String getFechaCaducidad() {
        return FechaCaducidad;
    }

    public void setFechaCaducidad(String FechaCaducidad) {
        this.FechaCaducidad = FechaCaducidad;
    }

    public float getTemperatura() {
        return Temperatura;
    }

    public void setTemperatura(float Temperatura) {
        this.Temperatura = Temperatura;
    }

    @Override
     public float calcularPago() {
        float incremento = 0;
        switch (this.getUnidadProducto()) {
            case 1: 
                incremento = 0.03f;
                break;
            case 2: 
                incremento = 0.05f;
                break;
            case 3: 
                incremento = 0.04f;
                break;
            
        }
        
      
        float Incremento1 = getPrecioUnitario() * (1 + incremento);
        
        float precioFinal = Incremento1 * 1.50f;
        
        return precioFinal;
    }
        
    
}
